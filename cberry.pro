TARGET = qcberry
TEMPLATE = lib
CONFIG += plugin

# PLUGIN_TYPE = platforms
# PLUGIN_CLASS_NAME = QMinimalIntegrationPlugin
# !equals(TARGET, $$QT_DEFAULT_QPA_PLUGIN): PLUGIN_EXTENDS = -

QT += core-private gui-private platformsupport-private

SOURCES =   main.cpp \
    qcberryintegration.cpp \
    qcberrybackingstore.cpp \
    cberry/ST7789.cpp \
    cberry/tft.cpp

HEADERS =   \
    qcberryintegration.h \
    qcberrybackingstore.h \
    cberry/ST7789.h \
    cberry/tft.h

LIBS += -lbcm2835

CONFIG += qpa/genericunixfontdatabase

OTHER_FILES += cberry.json
