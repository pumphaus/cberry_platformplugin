/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the plugins of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qcberryintegration.h"
#include "qcberrybackingstore.h"

#include <QtGui/private/qpixmap_raster_p.h>
#include <QtGui/private/qguiapplication_p.h>
#include <qpa/qplatformwindow.h>
#include <qpa/qplatformfontdatabase.h>

#include <QtPlatformSupport/private/qgenericunixfontdatabase_p.h>
#include <QtPlatformSupport/private/qgenericunixservices_p.h>
#include <QtPlatformSupport/private/qgenericunixeventdispatcher_p.h>

#include <QtPlatformSupport/private/qgenericunixeventdispatcher_p.h>

#include "cberry/tft.h"
#include "cberry/ST7789.h"
#include <bcm2835.h>

QT_BEGIN_NAMESPACE

QCBerryIntegration::QCBerryIntegration(const QStringList &parameters)
    : m_fontDb(new QGenericUnixFontDatabase),
      m_services(new QGenericUnixServices),
      m_nativeInterface(new QPlatformNativeInterface)
{
    Q_UNUSED(parameters)

    if (!bcm2835_init())
        qFatal("Failed to initialize BCM2835 library.");

    TFT_init_board();
    TFT_hard_reset();
    STcontroller_init();

    TFT_SetBacklightPWMValue( 256 );

    QCBerryScreen *mPrimaryScreen = new QCBerryScreen();

    mPrimaryScreen->mGeometry = QRect(0, 0, 320, 240);
    mPrimaryScreen->mDepth = 16;
    mPrimaryScreen->mFormat = QImage::Format_RGB16;

    screenAdded(mPrimaryScreen);
}

QCBerryIntegration::~QCBerryIntegration()
{
    bcm2835_close();
}

bool QCBerryIntegration::hasCapability(QPlatformIntegration::Capability cap) const
{
    switch (cap) {
    case ThreadedPixmaps: return true;
    case MultipleWindows: return true;
    default: return QPlatformIntegration::hasCapability(cap);
    }
}

QPlatformFontDatabase *QCBerryIntegration::fontDatabase() const
{
    return m_fontDb.data();
}

QPlatformServices *QCBerryIntegration::services() const
{
    return m_services.data();
}

QPlatformWindow *QCBerryIntegration::createPlatformWindow(QWindow *window) const
{
    QPlatformWindow *w = new QPlatformWindow(window);
    w->requestActivateWindow();
    return w;
}

QPlatformBackingStore *QCBerryIntegration::createPlatformBackingStore(QWindow *window) const
{
    return new QCBerryBackingStore(window);
}

QAbstractEventDispatcher *QCBerryIntegration::createEventDispatcher() const
{
    return createUnixEventDispatcher();
}

QPlatformNativeInterface *QCBerryIntegration::nativeInterface() const
{
    return m_nativeInterface.data();
}

QCBerryIntegration *QCBerryIntegration::instance()
{
    return static_cast<QCBerryIntegration *>(QGuiApplicationPrivate::platformIntegration());
}

QT_END_NAMESPACE
